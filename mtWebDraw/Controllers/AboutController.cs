﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mtWebDraw.Controllers
{
    public class AboutController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            string v = Request["v"];
            if (string.IsNullOrWhiteSpace(v))
                return View();
            else
                return View(v);
        }
	}
}